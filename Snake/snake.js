var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");
// Koordinaten der Schlange, der erste Eintrag ist der Kopf der Schlange
var snake = [{ x: 3, y: 2 }, { x: 4, y: 2 }, { x: 5, y: 2 }];
// Koordinaten der Frucht
var fruit = { x: 5, y: 5 };
// Richtung, in die sich die Schlange momentan bewegt
var currentDirection = { x: -1, y: 0 };
var GameOver = false;

var intervalID = setInterval(function () {
    // Movement and display
    var tail = moveSnake(snake, currentDirection);
    if (fruitCollidesWithSnake(snake, fruit)) {
        snake.push(tail);
        fruit = randomCoordinatesOutsideSnake(snake);
    }
    context.clearRect(0, 0, canvas.width, canvas.height);

    drawFruit();
    drawSnake();
    if (snakeHeadCollidesWithSnake(snake)) {
        GameOver = true;
    }
    // On Game over, stop setInterval
    if (GameOver) {
        clearInterval(intervalID);
        drawGameOver(snake.length - 3);
    }
}, 150);

document.body.addEventListener('keydown', function (event) {
    // nach links   {x: -1, y:  0}
    // nach rechts  {x:  1, y:  0}
    // nach oben    {x:  0, y: -1}
    // nach unten   {x:  0, y:  1}
    if (event.key === "ArrowLeft" && currentDirection.x != 1) {
        currentDirection = { x: -1, y: 0 };
    } else if (event.key === "ArrowRight" && currentDirection.x != -1) {
        currentDirection = { x: 1, y: 0 };
    } else if (event.key === "ArrowUp" && currentDirection.y != 1) {
        currentDirection = { x: 0, y: -1 };
    } else if (event.key === "ArrowDown" && currentDirection.y != -1) {
        currentDirection = { x: 0, y: 1 };
    }
});

function drawFruit() {
    context.fillStyle = '#538700';
    context.strokeStyle = '#d3d3d3'
    context.lineWidth = 2;
    context.fillRect(fruit.x * 20, fruit.y * 20, 20, 20);
    context.strokeRect(fruit.x * 20, fruit.y * 20, 20, 20);
}

function drawSnake() {
    context.fillStyle = '#000000';
    context.strokeStyle = '#d3d3d3';
    context.lineWidth = 2;
    // Draw the head seperatly, saves us a few checks while drawing the rest of the snake
    context.fillRect(snake[0].x * 20, snake[0].y * 20, 20, 20);
    context.strokeRect(snake[0].x * 20, snake[0].y * 20, 20, 20);
    context.fillStyle = '#00538E'
    for (var i = 1; i < snake.length; i++) {
        context.fillRect(snake[i].x * 20, snake[i].y * 20, 20, 20);
        context.strokeRect(snake[i].x * 20, snake[i].y * 20, 20, 20);
    }
}

function drawGameOver(points) {
    context.font = "50px Arial"
    context.fillStyle = "white";
    context.fillText("Game Over!", 70, 200);
    context.font = "25px Arial";
    context.fillText(points + " Punkte", 140, 230);
}

function fruitCollidesWithSnake(snake, fruit) {
    var result = false;
    for (var i = 0; i < snake.length; i++) {
        if (snake[i].x == fruit.x && snake[i].y == fruit.y) {
            result = true;
            break;
        }
    }
    return result;
}

function randomCoordinatesOutsideSnake(snake) {
    // Not the most efficient way but will definitely work
    // Just taking a random coordinate and checking if the snake would collide
    var point = { x: 0, y: 0 };
    point.x = Math.floor((Math.random() * 19) + 1);
    point.y = Math.floor((Math.random() * 19) + 1);
    while (fruitCollidesWithSnake(snake, point)) {
        point.x = Math.floor((Math.random() * 19) + 1);
        point.y = Math.floor((Math.random() * 19) + 1);
    }
    return point;
}

function snakeHeadCollidesWithSnake(snake) {
    var result = false;
    for (var i = 1; i < snake.length; i++) {
        if (snake[i].x == snake[0].x && snake[i].y == snake[0].y) {
            result = true;
            break;
        }
    }
    return result;
}

function moveSnake(snake, directionVector) {
    var result = snake.pop();
    var newX = mod(snake[0].x + directionVector.x, 20);
    var newY = mod(snake[0].y + directionVector.y, 20);
    var newHead = { x: newX, y: newY };
    snake.unshift(newHead);
    return result;
}

// Berechnet n modulo m
function mod(n, m) {
    return ((n % m) + m) % m;
}