#!/usr/bin/python3

import cgi
import cgitb

cgitb.enable()

# Zugriff auf alle uebermittelten Parameter
form = cgi.FieldStorage(encoding='utf8')
# Zugriff auf den Parameter (hier zuvor nach Integer konvertieren!)
try:
    n = int(form.getvalue('n'))
except:
    n = 0


def fib(i: int):
    if i == 1 or i == 2:
        return 1
    return fib(i - 1) + fib(i - 2)


invalid_input = False

if n < 1:
    invalid_input = True
    result = 0
else:
    result = fib(n)

print("Content-type: text/html")
print()
print("""
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<style>
</style>
</head>
<body>
""")
if invalid_input:
    print("""
    <h1>Error</h1>
    <p>Parameter n has not been given or it's invalid.</p>
    """)
else:
    print("""
    <h1>Fibonacchi</h1>
    <p>Die %d-te Fibonacci-Zahl lautet %d</p>
    """ % (n, result))
print("""
</body>
</html>
""")
