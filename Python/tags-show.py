#!/usr/bin/python3

import cgi
import json
import datetime
from os import listdir
from os.path import isfile, join
import sys


# enc_print zur Behebung des UTF-Problems mit dem Übungsserver
def enc_print(string='', encoding='utf8'):
    sys.stdout.buffer.write(string.encode(encoding) + b'\n')


form = cgi.FieldStorage()

key = form.getvalue('tag')

POSTS_PATH = 'posts/'
tag_list = []
post_list = []

onlyfiles = [f for f in listdir(POSTS_PATH) if isfile(join(POSTS_PATH, f))]
if key is None:
    for file in onlyfiles:
        open_file = open('posts/' + file)
        json_data = json.loads(open_file.read())
        tags = json_data['tags']
        for tag in tags:
            if tag not in tag_list:
                tag_list.append(tag)
else:
    for file in onlyfiles:
        open_file = open('posts/' + file)
        json_data = json.loads(open_file.read())
        tags = json_data['tags']
        for tag in tags:
            if tag == key:
                post_list.append(file)
                break

enc_print("Content-type: text/html")
enc_print()
enc_print("""
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="style.css">
<style>
body {text-align: center;}
</style>
</head>
<body>
""")
if key is None:
    enc_print("<h1>Tags</h1>")
    enc_print("<table>")
    for item in tag_list:
        enc_print("""
        <tr><td>
        <form action="tags-show.py" method="post"><button style="color: red;" name="tag" value="{}">{}</button>
        </form>
        </td></tr>    
        """.format(item,
                   item))
    enc_print("</table>")
else:
    enc_print("#" + key)
    for file in reversed(post_list):
        openfile = open('posts/' + file, 'r')
        data = openfile.read()
        post = json.loads(data)
        title = post['title']
        timestamp = post['published']
        readable_date = datetime.datetime.strptime(timestamp, "%Y-%m-%d-%H-%M-%S").strftime("%d.%m.%Y")
        readable_time = datetime.datetime.strptime(timestamp, "%Y-%m-%d-%H-%M-%S").strftime("%H.%M")
        actual_time = readable_date + ', ' + readable_time + " Uhr"
        content = post['content']
        tags = post['tags']
        tag_list = []
        for i in range(len(tags)):
            tag = tags[i]
            tag_list.append(tag)
        enc_print("""
            <table>    
            <tr><td style="font-size: large; font-weight: bold;">{}</td></tr>
            <tr><td style="font-size: small;">{}</td></tr>
            <tr><td style="font-size: medium;">{}</td></tr>
            <tr>
            <td style="color: red;">
            """.format(title,
                       actual_time,
                       content))
        for item in tag_list:
            enc_print(
                """
                <form action="tags-show.py" method="post"><button style="color: red;" name="tag" value="{}">{}</button>
                </form>
                """.format(item,
                           '#' + item + ' '))
        enc_print("""
            </td>
            </tr>
            </table>
            """)
enc_print("""
<a href="posts-create.py">Post hinzufügen</a><br />
<a style="margin-top: 2px" href="tags-show.py">Tags anzeigen</a>
</body>
</html>
""")
