#!/usr/bin/python3

import cgi
import cgitb
import time
import json
import sys


# enc_print zur Behebung des UTF-Problems mit dem Übungsserver
def enc_print(string='', encoding='utf8'):
    sys.stdout.buffer.write(string.encode(encoding) + b'\n')


cgitb.enable()
form = cgi.FieldStorage()
POSTS_PATH = 'posts/'
try:
    title = form.getvalue('Title')
    # title = "TESTTITLE"
    assert title is not None
    # Get all the inputs and convert them to the correct format
    content = form.getvalue('Content')
    # content = "TESTCONTENT"
    tags_input = form.getvalue('Tags')
    # tags_input = "#TESTTAG1#TESTTAG2"
    # Bit of string manipulation to get them to look right
    tags = str(tags_input).split('#')
    tags.pop(0)
    for i in range(len(tags)):
        tags[i] = tags[i].strip()
    timestamp = time.strftime('%Y-%m-%d-%H-%M-%S')

    data = {"title": title, "published": timestamp, "tags": tags, "content": content}

    filepath = POSTS_PATH + timestamp + '.JSON'
    with open(filepath, 'w') as posts:
        posts.write(str(json.dumps(data, indent=4)))
    # Redirect
    enc_print("Status: 303 See Other")
    enc_print("Location: {}".format('posts-show.py'))
    enc_print()

except Exception as e:
    enc_print('<h1>Error!</h1><p>Beim Speichern ist etwas schiefgegangen! Error: {}</p>'.format(e))
