#!/usr/bin/python3

import cgi
import cgitb
import random

cgitb.enable()

pic_amount = (random.randint(1, 7) * 3)

print("Content-type: text/html")
print()
print("""
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<style>
body {background-color: black}
div {column-count: 3; column-gap: 10px}
</style>
</head>
<body>
<h2 style=\"color:white\">Zufallsgalerie mit %d Bildern</h2>
<div>
""" % pic_amount)
for i in range(pic_amount):
    # Hier die Bilder von https://picsum.photos/ raussuchen
    URL = "https://picsum.photos/800/600?random=%d" % (i + 1)
    print("<img src=\"%s\">" % URL)
print("""
</div>
</body>
</html>
""")
