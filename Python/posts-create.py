#!/usr/bin/python3

import cgi
import sys


# enc_print zur Behebung des UTF-Problems mit dem Übungsserver
def enc_print(string='', encoding='utf8'):
    sys.stdout.buffer.write(string.encode(encoding) + b'\n')


enc_print("Content-type: text/html")
enc_print()
enc_print("""
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="style.css">
<style>
body {text-align: center;}
</style>
</head>
<body>
<h1>Post hinzufügen</h1>
<form action="posts-store.py" method="post">
<label for="Title">Titel</label>
<input type="text" name="Title" id="Title" required>
<br />
<label for="Content">Content</label>
<textarea name="Content" id="Content" required></textarea>
<br />
<label for="Tags">Tags</label>
<input type="text" name="Tags" id="Tags" required>
<br />
<button>Speichern</button>
</form>
</body>
</html>
""")
