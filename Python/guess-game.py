import numpy.random as np


def main():
    print("***************************************\n"
          "Willkommen zum lustigen Zahlenraten ...\n"
          "***************************************")
    to_guess = int(np.random() * 99)
    found = True
    tries = 0
    while found:
        user_guess = input("Bitte Zahl zwischen 0 und 99 eingeben: ")
        if user_guess.isnumeric() and 100 > int(user_guess) >= 0:
            user_int = int(user_guess)
            tries += 1
            if user_int == to_guess:
                print("Glueckwunsch, Zahl erraten. Anzahl Versuche: %d" % tries)
                return
            if user_int < to_guess:
                print("Die zu erratende Zahl ist groesser!")
            else:
                print("Die zu erratende Zahl ist kleiner!")


if __name__ == "__main__":
    # execute only if run as a script
    while True:
        main()
