var Canvas;
var Context;
var Player;
var Stones = [];
var StoneSize;
var initialize = true;
var CurrentDirection = { x: 0, y: 0 };
var Score;
var time;

var intervalID = setInterval(function () {
    if (initialize) {
        init();
        initialize = false;
    }
    if (Math.random() <= 0.05) {
        // Create a new Stone
        var newStone = CreateNewStone(Stones);
        Stones.push(newStone);
    }
    moveStones();
    // Clear context and redraw all Stones
    Context.clearRect(0, 0, Canvas.width, Canvas.height);
    drawStones();

    // Now, movement, like in snake, I'll save the next movement in a variable
    Player.x += CurrentDirection.x;
    Player.y += CurrentDirection.y;

    // Instant feedback! Who wants to wait until the end to know the results
    Score.innerHTML = "Zeit überlebt: " + (new Date().getTime() - time) / 1000;

    // Now check for collision
    if (!CheckCollision(Player) || OutofBounds()) {
        clearInterval(intervalID);
        Score.innerHTML = "Game over! Du hast " + (new Date().getTime() - time) / 1000 + " Sekunden durchgehalten!";
    }
}, 10);

document.body.addEventListener('keydown', function (event) {
    // nach links   {x: -1, y:  0}
    // nach rechts  {x:  1, y:  0}
    // nach oben    {x:  0, y: -1}
    // nach unten   {x:  0, y:  1}
    if (event.key === "ArrowLeft") {
        CurrentDirection = { x: -2, y: 0 };
    } else if (event.key === "ArrowRight") {
        CurrentDirection = { x: 2, y: 0 };
    } else if (event.key === "ArrowUp") {
        CurrentDirection = { x: 0, y: -2 };
    } else if (event.key === "ArrowDown") {
        CurrentDirection = { x: 0, y: 2 };
    }
});

document.body.addEventListener('keyup', function (event) {
    CurrentDirection = { x: 0, y: 0 }
});

function init() {
    Canvas = document.getElementById("canvas");
    Context = Canvas.getContext("2d");
    Player = { x: 100, y: 100 }
    GameOver = false;
    StoneSize = 10;
    Score = document.getElementById("score");
    time = new Date();
    time = time.getTime();

    // Drawing the player
    Context.fillStyle = '#FF0000';
    Context.fillRect(Player.x, Player.y, StoneSize, StoneSize);
    Context.fillStyle = '#FFFFFF';
    Context.strokeRect(0, 0, Canvas.width, Canvas.height);
}

function CreateNewStone(Stones) {
    // It's intentional that y coordinates are floats.
    var foundFreeRealEstate = false;
    var newStone = { x: 0, y: Math.random() * 390 }
    if (Stones.length == 0) {
        foundFreeRealEstate = true;
    }
    while (!foundFreeRealEstate) {
        newStone = { x: 0, y: Math.random() * 390 }
        for (var i = 0; i < Stones.length; i++) {
            if (CheckCollision(newStone)) {
                foundFreeRealEstate = true;
            }
        }
    }
    return newStone;
}

function moveStones() {
    for (var i = 0; i < Stones.length; i++) {
        Stones[i].x += 1;
        if (Stones[i].x + StoneSize > Canvas.width) {
            Stones.splice(i, 1);
        }
    }
}

function drawStones() {
    Context.strokeRect(0, 0, Canvas.width, Canvas.height);
    Context.fillStyle = '#FF0000';
    Context.fillRect(Player.x, Player.y, StoneSize, StoneSize);
    Context.fillStyle = '#000000';
    for (var i = 0; i < Stones.length; i++) {
        Context.fillRect(Stones[i].x, Stones[i].y, StoneSize, StoneSize);
    }
}

function CheckCollision(toCheck) {
    // Returns true if there is no collision being detected between Stones and Object toCheck
    var isGood = true;
    for (var i = 0; i < Stones.length; i++) {
        if (Math.abs(toCheck.x - Stones[i].x) < StoneSize && Math.abs(toCheck.y - Stones[i].y) < StoneSize) {
            isGood = false;
        }
    }
    return isGood;
}

function OutofBounds() {
    return Player.x < 0 | Player.x > Canvas.width - StoneSize | Player.y < 0 | Player.y > Canvas.height - StoneSize;
}