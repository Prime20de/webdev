<?php require_once(__DIR__ . "/hangman_lib.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Alle Wörter</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Alle Wörter</h1>
<?php
$words = getAllWords();
echo "<table>
<tr>
    <th>Wort</th>
    <th>Zu raten</th>
    <th>Maske</th>
</tr>";
// Hier jetzt alle Wörter einfügen
foreach($words as $word){
    echo "<tr>";
    echo "<td>".$word."</td>";
    echo "<td>".transformWord($word)."</td>";
    echo "<td>";
    foreach(maskWord($word) as $char){
        echo $char." ";
    }
    echo "</td>";
    echo "</tr>";
}
echo "</table>";
?>
</body>
</html>
