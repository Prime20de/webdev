<?php require_once(__DIR__ . "/hangman_lib.php");

session_name("zxmlr45u08");
session_start();

if (isset($_POST["letter"])) {
    guessLetter($_POST["letter"]);
}

header("Location: hangman.php");

?>