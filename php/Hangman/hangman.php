<?php require_once(__DIR__ . "/hangman_lib.php");

session_name("zxmlr45u08");
session_start();

if (!isset($_SESSION["toGuess"])) {
    //Redirect if not set
    header("Location: hangman-init.php");
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Wörter raten</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<h1>Wörter raten</h1>

<?php
$mask = $_SESSION["mask"];
foreach($mask as $char){
    echo $char." ";
}
?>

<form action="hangman-guess.php" method="post">
    <?php
    $alphabet = range('A', 'Z');
    foreach($alphabet as $letter){
        if (!in_array($letter, $_SESSION["guessedLetters"])){            
            echo "<button type=\"submit\" name=\"letter\" value=\"$letter\">$letter</button>";       
        }
    }
    ?>
</form>

<?php
$errors = $_SESSION["errorCount"];
$toGuess = $_SESSION["toGuess"];
echo "<p>Fehlversuche: $errors / 8</p>";
//State = 1 heißt gewonnen
if ($_SESSION["state"] == 1){
    echo "<p>Wort erraten!</p>";    
    echo "<a href=\"hangman-init.php\">Neues Spiel starten?</a>";
} elseif ($_SESSION["state"] == 2) {
    echo "<p>Wort leider nicht erraten, es war $toGuess</p>";
    echo "<a href=\"hangman-init.php\">Neues Spiel starten?</a>";
}
echo "<br />";
$errors = $errors <= 9 ? $errors : 9;
$fish = "fish/fish-".$errors.".svg";
echo "<img src=\"$fish\" width=\"800px\">";
?>

</body>
</html>
