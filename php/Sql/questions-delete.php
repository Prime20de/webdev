<?php require_once(__DIR__ . "/Database.php");

if (isset($_POST['id'])) {
    $db = new Database();
    $db->deleteQuestion($_POST['id']);
}

header("Location: questions-show.php");

?>