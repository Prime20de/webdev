<?php require_once(__DIR__ . "/Database.php");

if (isset($_POST["question"], $_POST["solution"], $_POST["answer0"], $_POST["answer1"], $_POST["answer2"])){
    $db = new Database();
    $db->addQuestion($_POST["question"], $_POST["answer0"], $_POST["answer1"], $_POST["answer2"], $_POST["solution"]);
}

header("Location: questions-show.php");

?>