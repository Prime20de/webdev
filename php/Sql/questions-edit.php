<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Meine Todo-Liste</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<h1>Fragen hinzufügen</h1>
<?php require_once(__DIR__ . "/Database.php");
$id = $_GET['id'];
$db = new Database();
$query = $db->queryQuestion($id);
$question = $query['question'];
$answer0 = $query['answer0'];
$answer1 = $query['answer1'];
$answer2 = $query['answer2'];
$solution = $query['solution'];

echo "<form action='questions-update.php' method='post'>
        <input type='hidden' name='id' value='".$id."'>

        <label for='question'>Frage:</label><br>
        <textarea id='question' name='question' required>".$question."</textarea><br>

        <label for='answer0'>Antwort 0:</label><br>
        <input type='radio' name='solution' value='0' ".(($solution==0)? "checked" : "")." required>
        <input type='text' id='answer0' name='answer0' value='$answer0' required><br>

        <label for='answer1'>Antwort 1:</label><br>
        <input type='radio' name='solution' value='1' ".(($solution==1)? "checked" : "")." required>
        <input type='text' id='answer1' name='answer1' value='$answer1' required><br>

        <label for='answer2'>Antwort 2:</label><br>
        <input type='radio' name='solution' value='2' ".(($solution==2)? "checked" : "")." required>
        <input type='text' id='answer2' name='answer2' value='$answer2' required><br>

        <button type='submit'>Frage speichern</button>
    </form>";
?>

</body>
</html>
