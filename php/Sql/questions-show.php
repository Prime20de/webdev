<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Meine Todo-Liste</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<h1>Fragen anzeigen</h1>

<table>
<tr>
<th>Frage</th>
<th>Antwort 0</th>
<th>Antwort 1</th>
<th>Antwort 2</th>
<th></th>
<th></th>
</tr>

<?php require_once(__DIR__ . "/Database.php");

$db = new Database();
$questions = $db->getQuestions();

foreach ($questions as $quest) {
    $question = $quest['question'];
    $id = $quest['id'];
    $answer0 = $quest['answer0'];
    $answer1 = $quest['answer1'];
    $answer2 = $quest['answer2'];
    $solution = $quest['solution'];
    echo "<tr>";
    echo "<td>$question</td>";
    echo "<td style='".(($solution==0)? "background-color: green;" : "background-color: red;")."'>$answer0</td>";
    echo "<td style='".(($solution==1)? 'background-color: green;' : 'background-color: red;')."'>$answer1</td>";
    echo "<td style='".(($solution==2)? 'background-color: green;' : 'background-color: red;')."'>$answer2</td>";
    echo "<td><form action='questions-edit.php' method='get'><button name='id' value='$id' type='submit'>Bearbeiten</button></form></td>";
    echo "<td><form action='questions-delete.php' method='post'><button name='id' value='$id' type='submit'>Löschen</button></form></td>";
    echo "</tr>";
}
?>
</table>

<p><a href="questions-create.php">Frage hinzufügen</a></p>

</body>
</html>
