<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Meine Todo-Liste</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<h1>Fragen hinzufügen</h1>

<form action="questions-store.php" method="post">
    <label for="question">Frage:</label><br>
    <textarea id="question" name="question" required></textarea><br>

    <label for="answer0">Antwort 0:</label><br>
    <input type="radio" name="solution" value="0" required>
    <input type="text" id="answer0" name="answer0" required><br>

    <label for="answer1">Antwort 1:</label><br>
    <input type="radio" name="solution" value="1" required>
    <input type="text" id="answer1" name="answer1" required><br>

    <label for="answer2">Antwort 2:</label><br>
    <input type="radio" name="solution" value="2" required>
    <input type="text" id="answer2" name="answer2" required><br>

    <button type="submit">Frage speichern</button>
</form>


</body>
</html>
