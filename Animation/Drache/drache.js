var BackgroundCounter;
var Canvas;
var Context;
var Dragon;
var BackgroundSpeed;
var DragonSpeed;
var DragonCounter;

function init() {
    Canvas = document.getElementById("canvas");
    Context = Canvas.getContext("2d");
    Dragon = document.getElementById("dragon");
    BackgroundCounter = 0;
    BackgroundSpeed = 7;
    DragonSpeed = 2;
    DragonCounter = 0;
    
    window.requestAnimationFrame(draw);
}

function draw() {
    BackgroundCounter += BackgroundSpeed;
    DragonCounter += DragonSpeed;
    if (BackgroundCounter > 700) BackgroundCounter = 0;
    if (DragonCounter > 700) DragonCounter = 0;
    // Background
    Context.drawImage(background, 0 - BackgroundCounter, 0);
    Context.drawImage(background, 700 - BackgroundCounter, 0);

    // Dragon animation
    var dragonHeight = Math.sin(2 * Math.PI * DragonSpeed * DragonCounter/700);
    Context.drawImage(dragon, 200, 100 + dragonHeight * 50, 169, 200);

    window.requestAnimationFrame(draw);
}